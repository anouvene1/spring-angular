package fr.springangular;

import fr.springangular.domain.Compte;
import fr.springangular.repositories.CompteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class CompteCommandLineRunner implements CommandLineRunner {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommandLineRunner.class);
    private final CompteRepository repository;

    public CompteCommandLineRunner(CompteRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run(String... args) throws Exception {
        repository.save(new Compte(2741, "Prêts participatifs"));
        repository.save(new Compte(2742, "Prêts aux associés"));
        repository.save(new Compte(2743, "Prêts au personnel"));
        repository.save(new Compte(2744, "Prêts divers"));
//        repository.findAll().forEach(System.out::println);
    }
}
