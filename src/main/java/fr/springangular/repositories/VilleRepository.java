package fr.springangular.repositories;

import fr.springangular.domain.Pays;
import fr.springangular.domain.Personne;
import fr.springangular.domain.Ville;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource
public interface VilleRepository extends JpaRepository<Ville, Long> {
    List<Ville> findAllByPays(Pays pays);
}
