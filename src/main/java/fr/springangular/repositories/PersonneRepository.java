package fr.springangular.repositories;

import fr.springangular.domain.Pays;
import fr.springangular.domain.Personne;
import fr.springangular.domain.Ville;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonneRepository extends JpaRepository<Personne, Long> {
    List<Personne> findAllByPays(Pays pays);

    List<Personne> findAllByVille(Ville ville);
    List<Personne> findAllByVilleStartsWith(String word);
}
