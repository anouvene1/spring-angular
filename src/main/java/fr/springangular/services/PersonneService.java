package fr.springangular.services;

import fr.springangular.domain.Pays;
import fr.springangular.domain.Personne;
import fr.springangular.domain.Ville;
import fr.springangular.entityWrapper.dto.PersonneDTO;
import fr.springangular.entityWrapper.mapper.PersonneMapper;
import fr.springangular.repositories.PaysRepository;
import fr.springangular.repositories.PersonneRepository;
import fr.springangular.repositories.VilleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonneService {

    @Autowired
    private VilleRepository villeRepository;

    @Autowired
    private PaysRepository paysRepository;

    @Autowired
    private PersonneRepository personneRepository;

    @Autowired
    private PersonneMapper personneMapper;

    /**
     * Retreive the pezrsons list by country or by city
     * @param id Identifier of county or city
     * @param key String key "county" or "city"
     * @return List<PersonneDTO> persons list
     */
    public List<PersonneDTO> retreivePersonsByCountryOrCity(Long id, String key) {
        List<Personne> personnes =  new ArrayList<>();
        List<PersonneDTO> personnesDTO = new ArrayList<>();

        if(key.equals("country")) {
            Pays p = this.paysRepository.findById(id).orElse(null);
            personnes =  this.personneRepository.findAllByPays(p);
        } else if(key.equals("city")) {
            Ville v = this.villeRepository.findById(id).orElse(null);
            personnes =  this.personneRepository.findAllByVille(v);
        }

        for(Personne personne: personnes) {
            personnesDTO.add(this.convertToDTO(personne));
        }

        return personnesDTO;
    }

    /**
     * Converter Personne to PersonneDTO
     * @param person Person to convert
     * @return PersoneDTO
     */
    private PersonneDTO convertToDTO(Personne person) {
        PersonneDTO personneDTO = this.personneMapper.map(person, PersonneDTO.class);
        return personneDTO;
    }

}
