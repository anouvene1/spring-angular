package fr.springangular.exceptions;

public class CompteNotFoundException extends RuntimeException {
    public CompteNotFoundException(Long id) {
        super("Compte inexistant !!" + id);
    }
}
