package fr.springangular.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class AddCompteException extends RuntimeException {
    public AddCompteException(String message) {
        super(message);
    }
}
