package fr.springangular;

import fr.springangular.domain.Pays;
import fr.springangular.domain.Personne;
import fr.springangular.domain.Ville;
import fr.springangular.repositories.PaysRepository;
import fr.springangular.repositories.PersonneRepository;
import fr.springangular.repositories.VilleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PersonneCommandLineRunner implements CommandLineRunner {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommandLineRunner.class);
    private final PersonneRepository personneRepository;

    @Autowired
    private PaysRepository pr;
    @Autowired
    private VilleRepository vr;

    public PersonneCommandLineRunner(PersonneRepository repository) {
        this.personneRepository = repository;
    }

    @Override
    public void run(String... args) throws Exception {
        List<Pays> lesPays = new ArrayList<>();
        List<Ville> lesVillesParPays = new ArrayList<>();

        // les pays persistés
        lesPays = this.pr.findAll();

        // les villes persistées
        lesVillesParPays = this.vr.findAllByPays(lesPays.get(0));

        if(!lesPays.isEmpty() && !lesVillesParPays.isEmpty()) {
            this.personneRepository.save(new Personne(
                    "NGUYEN",
                    "02.65.14.56.98",
                    "ng@gmail.com",
                    "45 Avenue Foch",
                    "75001",
                    lesVillesParPays.get(0),
                    lesPays.get(0)));
            this.personneRepository.save(new Personne(
                    "MINH",
                    "07.65.14.22.98",
                    "ng@gmail.com",
                    "45 Avenue Foch",
                    "69002",
                    lesVillesParPays.get(1),
                    lesPays.get(0)));
            this.personneRepository.save(new Personne(
                    "KLEIN",
                    "04.74.32.56.88",
                    "kklein@gmail.com",
                    "45 rue du peuple",
                    "06088",
                    lesVillesParPays.get(3),
                    lesPays.get(4)));
            this.personneRepository.save(new Personne(
                    "MEUNIER",
                    "08.74.32.56.88",
                    "mplein@gmail.com",
                    "78 Ramos Lilas",
                    "06088",
                    lesVillesParPays.get(2),
                    lesPays.get(1)));
            this.personneRepository.save(new Personne(
                    "RUAN",
                    "08.74.32.56.88",
                    "mplein@gmail.com",
                    "78 Ramos Lilas",
                    "06088",
                    lesVillesParPays.get(3),
                    lesPays.get(2)));
            this.personneRepository.save(new Personne(
                    "KIUN",
                    "33.74.32.36.88",
                    "mplein@gmail.com",
                    "08 Pin To",
                    "06088",
                    lesVillesParPays.get(3),
                    lesPays.get(3)));
        }

        // personneRepository.findAll().forEach(System.out::println);
    }
}
