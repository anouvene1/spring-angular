package fr.springangular.controllers;

import fr.springangular.domain.Compte;
import fr.springangular.exceptions.AddCompteException;
import fr.springangular.exceptions.CompteNotFoundException;
import fr.springangular.repositories.CompteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class CompteController {
    private CompteRepository repository;
    private static Logger log = LoggerFactory.getLogger(CompteController.class);

    public CompteController(CompteRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/api/lescomptes")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<Compte> retreiveComptes() {
        return this.repository.findAll();
    }

//    @GetMapping("/lescomptes/{id}")
//    @CrossOrigin(origins = "http://localhost:4200")
//    public Optional<Compte> retreiveCompteById(@PathVariable Long id) {
//        return this.repository.findById(id);
//    }

    @GetMapping("/api/lescomptes/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public Compte retreiveCompteById(@PathVariable Long id) {
        return this.repository.findById(id)
                .orElseThrow(() -> new CompteNotFoundException(id));
    }

    @PostMapping(path = "/api/lescomptes", consumes = "application/json", produces = "application/json")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Compte> addCompte(@RequestBody Compte compte) {
        Compte newCompte = this.repository.save(compte);
        if (newCompte == null) {
            throw new AddCompteException("Impossible de créer le compte!");
        }

        return new ResponseEntity<>(newCompte, HttpStatus.CREATED); // 201: Requête traitée avec succès et création d’un document.
    }

//    @PutMapping(path = "/api/lescomptes/{id}", consumes = "application/json", produces = "application/json")
//    @CrossOrigin(origins = "http://localhost:4200")
//    public ResponseEntity<Compte> updateCompte(@PathVariable Long id, @RequestBody Compte newCompte) {
//        Compte compteToUpdate = this.retreiveCompteById(id).orElse(null); // orElse: methode de Optionnal
//
//        log.info("-----------------" + compteToUpdate + "------------------");
//
//        if(compteToUpdate == null) {
//            throw new AddCompteException("Impossible de mettre à jour le compte!");
//        } else {
//            compteToUpdate.setNum(newCompte.getNum());
//            compteToUpdate.setNom(newCompte.getNom());
//
//            Compte compte = this.repository.save(compteToUpdate);
//            return new ResponseEntity<>(compte, HttpStatus.NO_CONTENT); // 204 : Requête traitée avec succès mais pas d’information à renvoyer.
//        }
//    }

    @PutMapping(path = "/api/lescomptes/{id}", consumes = "application/json", produces = "application/json")
    @CrossOrigin(origins = "http://localhost:4200")
    public Compte updateCompte(@PathVariable Long id, @RequestBody Compte newCompte) {

        return this.repository.findById(id)
                .map(compte -> {
                    compte.setNum(newCompte.getNum());
                    compte.setNom(newCompte.getNom());
                    return this.repository.save(compte);
                }).orElseGet(() -> {
                    newCompte.setId(id);
                    return this.repository.save(newCompte);
                });
    }

    @DeleteMapping(path = "/api/lescomptes/{id}", consumes = "application/json", produces = "application/json")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Compte> deleteCompteById(@PathVariable Long id) {
        Compte compteToDelete = this.retreiveCompteById(id);

        if(compteToDelete != null) {
            this.repository.delete(compteToDelete);
            return new ResponseEntity<>(compteToDelete, HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(compteToDelete, HttpStatus.BAD_REQUEST);
    }



}