package fr.springangular.controllers;

import fr.springangular.domain.Pays;
import fr.springangular.repositories.PaysRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PaysController {

    @Autowired
    private PaysRepository pr;

    @GetMapping("/api/lespays")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<Pays> retreivePays() {
        return this.pr.findAll();
    }
}
