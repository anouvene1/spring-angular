package fr.springangular.controllers;

import fr.springangular.domain.Personne;
import fr.springangular.entityWrapper.dto.PersonneDTO;
import fr.springangular.entityWrapper.mapper.PersonneMapper;
import fr.springangular.exceptions.AddPersonneException;
import fr.springangular.repositories.PersonneRepository;
import fr.springangular.services.PersonneService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class PersonneController {
    @Autowired
    private PersonneRepository pr;

    @Autowired
    private PersonneService ps;

    @Autowired
    private PersonneMapper personneMapper;

    private static Logger log = LoggerFactory.getLogger(CompteController.class);

    @GetMapping("/api/lespersonnes")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<Personne> retreivePersonnes() {
        return this.pr.findAll();
    }

    /**
     * POST Personne
     * @param p Personne to post
     * @return ResponseEntity with the status code 201
     */
    @PostMapping(value = "api/lespersonnes", consumes = "application/json", produces = "application/json")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Personne> createPersonne(@RequestBody Personne p) {
        Personne addedPersonne = this.pr.save(p);
        if(addedPersonne == null) {
            throw new AddPersonneException("Impossible de créer le client, veuiller recommencer");
        }

        return new ResponseEntity<>(addedPersonne, HttpStatus.CREATED);
    }

    // Récupérer les personnes par ville
    @GetMapping("/api/lespersonnes/ville/{cityId}")
    public List<PersonneDTO> byCity(@PathVariable Long cityId) {
        return this.ps.retreivePersonsByCountryOrCity(cityId, "city");
    }

    // Récupérer les personnes par pays
    @GetMapping("/api/lespersonnes/pays/{paysId}")
    public List<PersonneDTO> byCountry(@PathVariable Long paysId) {
        return this.ps.retreivePersonsByCountryOrCity(paysId, "country");
    }







}
