package fr.springangular.controllers;

import fr.springangular.domain.Pays;
import fr.springangular.domain.Ville;
import fr.springangular.repositories.PaysRepository;
import fr.springangular.repositories.VilleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class VilleController {
    @Autowired
    private VilleRepository vr;

    @Autowired
    private PaysRepository pr;

    @GetMapping("/api/lesvilles")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<Ville> retreiveVilles() {
        return this.vr.findAll();
    }

    @GetMapping("/api/lesvilles/{paysId}")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<Ville> retreiveVillesByPays(@PathVariable Long paysId) throws Exception {
        System.out.println("===========" + paysId + "================");
        Pays p = this.pr.findById(paysId).orElse(null);
        if(p == null){
            throw new Exception("Not found");
        }
        return this.vr.findAllByPays(p);
    }

}
