package fr.springangular;

import fr.springangular.domain.Pays;
import fr.springangular.domain.Ville;
import fr.springangular.repositories.PaysRepository;
import fr.springangular.repositories.VilleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PaysCommandLineRunner implements CommandLineRunner {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommandLineRunner.class);
    private final PaysRepository pr;

    @Autowired
    private VilleRepository vr;

    public PaysCommandLineRunner(PaysRepository repository) {
        this.pr = repository;
    }

    @Override
    public void run(String... args) throws Exception {
        List<Ville> lesVillesParPays = new ArrayList<>();
        List<Pays> lesPays = new ArrayList<>();

        // ====== Pays ======
        String[] paysName = {"France", "Italie", "Espagne", "Portugal", "Suède"};
        this.persisterPays(paysName);

        // les pays persistés
        lesPays = this.pr.findAll();
        LOGGER.info("============== Les pays: " + lesPays.get(0).getNom() + " ======================");

        // ===== Villes =====

        // France
        String[] villesName_fr = {"Paris", "Lyon", "Marseille", "Nice"};
        this.persisterVilles(lesPays.get(0),  villesName_fr);

        // les villes persistées
        lesVillesParPays = this.vr.findAllByPays(lesPays.get(0));
        LOGGER.info("============== Les villes de France: " + lesVillesParPays.get(0).getNom() + " ======================");

        // Italie
        String[] villesName_it = {"Rome", "Naples", "Milan", "Florence", "Turin"};
        this.persisterVilles(lesPays.get(1),  villesName_it);

        // Espagne
        String[] villesName_es = {"Madrid", "Barcelone", "Séville", "Madrid", "Alma"};
        this.persisterVilles(lesPays.get(2),  villesName_es);

        // Portugal
        String[] villesName_pt = {"Lisbone", "Porto", "Amadora", "Funchal", "Porto"};
        this.persisterVilles(lesPays.get(3),  villesName_pt);

        // Suède
        String[] villesName_se = {"Stockholm", "Göteborg", "Malmö", "Västeras", "Orebro"};
        this.persisterVilles(lesPays.get(4),  villesName_se);

        // this.pr.findAll().forEach(System.out::println);
    }

    /**
     * Persister les pays
     * @param paysName Nom du pays
     */
    private void persisterPays(String[] paysName) {
        for(String pn: paysName) {
            Pays p = new Pays(pn);
            this.pr.save(p);
        }
    }

    /**
     * Persister les villes dans chaque pays
     * @param pays Nom du pays
     * @param villesName Nom de la ville
     */
    private void persisterVilles(Pays pays, String[] villesName) {
        for(String vn: villesName) {
            Ville v = new Ville(vn);
            v.setPays(pays);
            this.vr.save(v);
        }
    }
}
