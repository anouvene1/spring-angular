package fr.springangular;

import fr.springangular.domain.Pays;
import fr.springangular.domain.Ville;
import fr.springangular.repositories.PaysRepository;
import fr.springangular.repositories.VilleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class VilleCommandLineRunner implements CommandLineRunner {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommandLineRunner.class);
    private final VilleRepository vr;

    @Autowired
    private PaysRepository pr;
    private List<Pays> pays = new ArrayList<>();

    
    public VilleCommandLineRunner(VilleRepository vr) {
        this.vr = vr;
    }

    @Override
    public void run(String... args) throws Exception {
//        this.pays = pr.findAll();
//        if(!pays.isEmpty()) {
//
//            this.vr.save(new Ville("Paris"));
//            this.vr.save(new Ville("Lyon"));
//            this.vr.save(new Ville("Barcelone"));
//            this.vr.save(new Ville("Rome"));
//            this.vr.save(new Ville("Lyon"));
//            this.vr.save(new Ville("Stockholm"));
//            this.vr.findAll().forEach(System.out::println);
//
//        }
    }
}
