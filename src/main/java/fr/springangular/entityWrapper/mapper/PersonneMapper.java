package fr.springangular.entityWrapper.mapper;

import fr.springangular.domain.Personne;
import fr.springangular.entityWrapper.dto.PersonneDTO;
import org.springframework.stereotype.Component;

import org.modelmapper.Conditions;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

@Component
public class PersonneMapper extends ModelMapper {

    public PersonneMapper() {
        this.createCustomMapper();
    }

    private void createCustomMapper() {
        this.getConfiguration().setPropertyCondition(Conditions.isNotNull());

        PropertyMap<Personne, PersonneDTO> mappingToDTO = new PropertyMap<Personne, PersonneDTO>() {
            @Override
            protected void configure() {
                map(source.getAdresse()).setAdresse(null);
                map(source.getComptes()).setComptes(null);
                map(source.getCp()).setCp(null);
                map(source.getEmail()).setEmail(null);
                map(source.getNom()).setNom(null);
                map(source.getTel()).setTel(null);
                map(source.getVille()).setVille(null);
                map(source.getPays()).setPays(null);
            }
        };

        this.addMappings(mappingToDTO);
        this.validate();

    }
}
