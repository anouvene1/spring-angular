package fr.springangular.entityWrapper.dto;

import fr.springangular.domain.Ville;
import io.swagger.annotations.ApiModelProperty;

import fr.springangular.domain.Compte;
import fr.springangular.domain.Pays;


import java.util.List;

public class PersonneDTO {
    @ApiModelProperty(notes = "Person id")
    private Long id;

    @ApiModelProperty(notes = "Person lastname")
    private String nom;

    @ApiModelProperty(notes = "Person phone number")
    private String tel;

    @ApiModelProperty(notes = "Person e-mail")
    private String email;

    @ApiModelProperty(notes = "Person address")
    private String adresse;

    @ApiModelProperty(notes = "Person postal code")
    private String cp;

    @ApiModelProperty(notes = "Person account")
    private List<Compte> comptes;

    @ApiModelProperty(notes = "Person city")
    private Ville ville;

    @ApiModelProperty(notes = "Person country")
    private Pays pays;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public List<Compte> getComptes() {
        return comptes;
    }

    public void setComptes(List<Compte> comptes) {
        this.comptes = comptes;
    }

    public Ville getVille() {
        return ville;
    }

    public void setVille(Ville ville) {
        this.ville = ville;
    }

    public Pays getPays() {
        return pays;
    }

    public void setPays(Pays pays) {
        this.pays = pays;
    }
}
