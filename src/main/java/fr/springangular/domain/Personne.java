package fr.springangular.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;

@Entity
public class Personne {
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @NotEmpty(message = "Nom ne doit pas être vide")
    private String nom;

    @NotEmpty(message = "Tel ne doit pas être vide")
    private String tel;

    @NotNull
    @NotEmpty
    @Pattern(regexp="[A-Za-z0-9._%-+]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}")
    private String email;

    @NotBlank(message = "Pas de blanc")
    private String adresse;

    @NotNull(message = "Code postal ne doit pas être nul")
    private String cp;

    @OneToMany(mappedBy = "personne")
    private List<Compte> comptes;

    @ManyToOne(fetch = FetchType.EAGER)
    private Pays pays;

    @ManyToOne(fetch = FetchType.EAGER)
    private Ville ville;

    public Personne() { }

    public Personne(
            String nom,
            String tel,
            String email,
            String adresse,
            String cp,
            Ville ville,
            Pays pays
    ) {
        this.nom = nom;
        this.tel = tel;
        this.email = email;
        this.adresse = adresse;
        this.cp = cp;
        this.ville = ville;
        this.pays = pays;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getTel() {
        return tel;
    }
    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdresse() {
        return adresse;
    }
    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCp() {
        return cp;
    }
    public void setCp(String cp) {
        this.cp = cp;
    }

    public List<Compte> getComptes() {
        return comptes;
    }
    public void setComptes(List<Compte> comptes) {
        this.comptes = comptes;
    }

    public Ville getVille() {
        return ville;
    }
    public void setVille(Ville ville) {
        this.ville = ville;
    }

    public Pays getPays() {
        return pays;
    }
    public void setPays(Pays pays) {
        this.pays = pays;
    }
}
