package fr.springangular.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Pays {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nom;

    @OneToMany(mappedBy = "pays")
    @JsonIgnore
    private List<Personne> personnes = new ArrayList<>();

    @OneToMany(mappedBy = "pays")
    @JsonIgnore
    private List<Ville> villes = new ArrayList<>();

    public Pays() { }

    public Pays(String nom) {
        this.nom = nom;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) { this.id = id; }

    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Ville> getVilles() { return villes; }
    public void setVilles(List<Ville> villes) { this.villes = villes; }
    public void addVille(Ville ville) { this.villes.add(ville); }

    public List<Personne> getPersonnes() { return personnes; }
    public void setPersonnes(List<Personne> personnes) { this.personnes = personnes; }
    public void addPersonne(Personne p) { this.personnes.add(p); }
}
